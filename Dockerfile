FROM my-ubuntu:latest
MAINTAINER Kaka Puzikova <gitlab@tost.info.tm>

RUN echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA2xOQvstqaxbGt5J8nC+5AnncdLlFuIANPmBtXbw6YISQxHwb3cYiyjqMS4+h0eUwukG3r1BaFQ5BeDAr0t5xxKs+YjHUwOD4hP5JMRfoOdxxTLNWqGvDNiJyc2qtEivt1z+gReK42vE1T8+yc6IKObkGGKey4nNYHl7K4QJryJV0UzsO4MPmDYZ3AhaP+K4Og3qIYX9tu5fAVgKJvHBtdjmTo3cf9VBhySnuVSgMBJiW4KCqBNP905yKBjN0J+IIm17vdjaYM+YAbrbYLwwKd/HvBn5ww0w/UedyFi68h0f2zitlhkfDj8wU0FpQRSZQ/ViVDn++R6YiCwjc3J71BQ==" \
    > /admin.pub

# Install packages
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
    apt-get -y install \
	git \
	ssh \
	sudo

#	git-arch \
#	git-doc \
#	git-email \
#	git-man \

RUN useradd -c 'Git user' -m -U -s /bin/bash git

RUN sudo -u git bash -c 'export HOME="/home/git";cd ${HOME};\
    git clone git://github.com/sitaramc/gitolite;\
    mkdir -p ${HOME}/bin;\
    gitolite/install -to ${HOME}/bin;\
    mkdir ${HOME}/.ssh;\
    ${HOME}/bin/gitolite setup -pk /admin.pub;'

RUN sed -ie 's/^AcceptEnv/#AcceptEnv/' /etc/ssh/sshd_config;

RUN mkdir /root/.ssh \
    && cat /admin.pub > /root/.ssh/authorized_keys \
    && chmod 700 /root/.ssh \
    && chmod 600 /root/.ssh/*

RUN find /var/log -type f -exec rm -f '>{}' \;
RUN find /var/cache/apt/archives/ -type f -name '*.deb' -exec rm -f '>{}' \;


VOLUME  ["/home/git"]

EXPOSE 22

CMD chown -R git:git /home/git && service ssh start && tail -F /var/log/system.log
