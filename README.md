# Docker / my-gitolite
Простой имидж gitolite+ubuntu.
## Получаем репу
```bash
git clone git@gitlab.com:kapuza-docker/my-gitolite.git
cd gitolite
git config user.name "Kaka Puzikova"; git config user.email "gitlab@tost.info.tm"
```
## Использование
```bash
# Сборка имиджа (если он уже склонирован)
docker build -t my-git .

# Можно прям из репы (если лень клонировать)
docker build -t my-git git@gitlab.com:kapuza-docker/my-gitolite.git

# Создаем контейнер
docker run -d -P --name git my-git

# Смотрим на каком порту поднялся git (например 32782)
docker ps

# Глянем наши репы. Вместо localhost можно имя сервера с докером.
ssh -p 32782 git@localhost

# Закачаем gitolite-admin https://github.com/sitaramc/gitolite
git clone ssh://git@docker-server:32782/gitolite-admin
```
